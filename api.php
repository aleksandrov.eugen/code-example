<?php

use App\Http\Controllers\ApiClimateEventsController;
use App\Http\Controllers\ApiContentProxyController;
use App\Http\Controllers\ApiPublicationsController;
use App\Http\Controllers\ApiSearchController;
use Illuminate\Support\Facades\Route;
use App\Models\Article;
use App\Models\Gallery;
use App\Models\Post;
use App\Models\Video;
use App\Models\VideoCategory;
use App\Models\VideoCycle;
use App\Models\ArticleCategory;
use App\Models\PostCategory;
use App\Models\StreamEvent;
use App\Http\Controllers\ApiArticlesController;
use App\Http\Controllers\ApiFormsController;
use App\Http\Controllers\ApiTranslationsController;
use App\Http\Controllers\ApiPostsController;
use App\Http\Controllers\ApiVideosController;
use App\Http\Controllers\ApiRedirectsController;
use App\Http\Controllers\ApiGalleriesController;
use App\Http\Controllers\ApiStreamsController;
use App\Http\Controllers\ApiConferencesController;
use App\Services\ConferenceService;

Route::prefix('v1')->group(static function () {

    // Lang dependant routes
    Route::middleware('lang.required')->group(callback: static function () {

        //Translations list --> api/v1/translations?lang=ru
        Route::get('translations', [ApiTranslationsController::class, 'list']);

        //Articles --> api/v1/articles?lang=ru
        Route::prefix('articles')->group(static function () {
            Route::bind('articleCategorySlug', static function ($slug) {
                return ArticleCategory::publishedByRequestLang()->bySlug($slug)->firstOrFail();
            });

            Route::bind('articleSlug', static function ($value) {
                return Article::publishedByRequestLang()->bySlug($value)->firstOrFail();
            });

            //List articles paginator
            Route::get('/', [ApiArticlesController::class, 'list']);

            // List articles for climate page
            Route::get('/climate-page', [ApiArticlesController::class, 'climatePageArticleList']);

            //Category Articles list with Category Data
            Route::get('category/{articleCategorySlug}', [ApiArticlesController::class, 'categoryList']);

            //Single article page
            Route::get('{articleSlug}', [ApiArticlesController::class, 'singleArticle']);

            // Article comments
            Route::get('{articleSlug}/comments', [ApiArticlesController::class, 'listComments']);
            Route::post('{articleSlug}/comments', [ApiArticlesController::class, 'addComment']);
        });

        //News = Posts --> api/v1/news?lang=ru
        Route::prefix('news')->group(static function () {
            Route::bind('postCategorySlug', static function ($slug) {
                return PostCategory::publishedByRequestLang()->bySlug($slug)->firstOrFail();
            });

            Route::bind('postSlug', static function ($value) {
                return Post::publishedByRequestLang()->bySlug($value)->firstOrFail();
            });

            //List news paginator
            Route::get('/', [ApiPostsController::class, 'list']);

            //Homepage Videos --> api/v1/videos/homepage?lang=ru
            Route::get('homepage', [ApiPostsController::class, 'homepageList']);

            //Category Posts list with Category Data
            Route::get('category/{postCategorySlug}', [ApiPostsController::class, 'categoryList']);

            //Single post page
            Route::get('{postSlug}', [ApiPostsController::class, 'singlePost']);

            // Post comments
            Route::get('{postSlug}/comments', [ApiPostsController::class, 'listComments']);
            Route::post('{postSlug}/comments', [ApiPostsController::class, 'addComment']);
        });

        //Videos --> api/v1/videos?lang=ru
        Route::prefix('videos')->group(static function () {
            Route::bind('videoCategorySlug', static function ($slug) {
                return VideoCategory::publishedByRequestLang()->bySlug($slug)->firstOrFail();
            });

            Route::bind('videoSlug', static function ($value) {
                return Video::publishedByRequestLang()->bySlug($value)->firstOrFail();
            });

            Route::bind('categoryMasterId', static function ($id) {
                return VideoCategory::publishedByRequestLang()->where('master_id', $id)->firstOrFail();
            });

            Route::bind('cycleMasterId', static function ($id) {
                return VideoCycle::publishedByRequestLang()->where('master_id', $id)->firstOrFail();
            });

            //Main Videos page = List of Categories grouped by Parent Category
            Route::get('programs', [ApiVideosController::class, 'programsList']);

            //Homepage Videos --> api/v1/videos/homepage?lang=ru
            Route::get('homepage', [ApiVideosController::class, 'homepageList']);

            // Videos with Scientists for Climate page
            Route::get('/climate-page/scientists', [ApiVideosController::class, 'climatePageScientistsVideos']);

            // Popular Videos for Climate page
            Route::get('/climate-page/popular', [ApiVideosController::class, 'climatePagePopularVideos']);

            //Category Videos list with Category Data
            Route::get('category/{videoCategorySlug}', [ApiVideosController::class, 'categoryList']);

            //Single Video page
            Route::get('{videoSlug}', [ApiVideosController::class, 'singleVideo']);

            //Videos for Conference page old site /byCategoryAndCycleMasters/{categoryMasterId}/{cycleMasterId}
            Route::get('category-cycle-masters/{categoryMasterId}/{cycleMasterId}', [ApiVideosController::class, 'categoryCycleList']);

            // Cycles list for Category by videoCategorySlug. On old site /en/api/videos/cycles/conference
            Route::get('cycles-by-category/{videoCategorySlug}', [ApiVideosController::class, 'cyclesByCategory']);

            // Video comments
            Route::get('{videoSlug}/comments', [ApiVideosController::class, 'listComments']);
            Route::post('{videoSlug}/comments', [ApiVideosController::class, 'addComment']);
        });

        // Conferences --> api/v1/conferences/{conferenceSlug}
        Route::prefix('conferences')->group(static function () {
            Route::bind('conferenceSlug', static function ($slug) {
                // Get Conference for current or default Lang
                return app(ConferenceService::class)->getConferenceByRequestOrDefaultLang($slug);
            });

            //Streams for Conference page
            Route::get('{conferenceSlug}/streams', [ApiConferencesController::class, 'streams']);

            //Videos by Categories for Conference page
            Route::get('{conferenceSlug}/videos', [ApiConferencesController::class, 'videos']);
        });

        // TODO: Удалить после перехода на новый фронтенд (нужен только для старого сайта)
        Route::prefix('temp')->group(static function () {
            Route::get('/get-news-or-article/{slug}', [ApiContentProxyController::class, 'getNewsOrArticle']);
        });

        Route::get('/about-project-gallery', [ApiGalleriesController::class, 'aboutProjectGallery']);

        Route::get('/search', [ApiSearchController::class, 'index']);
    });

    Route::prefix('climate-events')->group(static function () {
        Route::get('/points', [ApiClimateEventsController::class, 'points']);
    });

    Route::prefix('publications')->group(static function () {
        Route::get('/', [ApiPublicationsController::class, 'list']);
        Route::get('/langs', [ApiPublicationsController::class, 'langs']);
    });

    //Gallery images --> api/v1/galleries/{gallerySlug}
    Route::prefix('galleries')->group(static function () {
        Route::bind('gallerySlug', static function ($slug) {
            return Gallery::published()->bySlug($slug)->firstOrFail();
        });

        //Gallery Images by slug
        Route::get('{gallerySlug}', [ApiGalleriesController::class, 'imagesList']);
    });

    // Streams by Event for Conference widget
    Route::prefix('streams')->group(static function () {
        Route::bind('widgetStream', static function ($id) {
            return StreamEvent::published()->findOrFail($id);
        });

        // /api/v1/streams/homepage-widget
        Route::get('/homepage-widget', [ApiStreamsController::class, 'homepageWidget']);

        // api/v1/streams/{id}/widget
        Route::get('/{widgetStream}/widget', [ApiStreamsController::class, 'widget']);
    });

    Route::prefix('forms')->group(static function () {
        Route::post('/climate-page/join', [ApiFormsController::class, 'climatePageJoin']);
    });

    Route::get('/translations/langs', [ApiTranslationsController::class, 'langList']);

    Route::get('/redirect', [ApiRedirectsController::class, 'item']);
});
