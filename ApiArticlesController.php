<?php

namespace App\Http\Controllers;

use App\DTO\ApiListResponse;
use App\Http\Requests\ArticlesListRequest;
use App\Http\Requests\ListComments;
use App\Http\Requests\StoreComment;
use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Comment;
use App\Services\ArticleService;
use App\Services\ClimatePageService;
use App\Services\CommentService;
use App\Transformers\Article\ListTransformer;
use App\Transformers\Article\SingleArticleTransformer;
use App\Transformers\Common\CategoryTransformer;
use App\Transformers\Comment\ListTransformer as CommentListTransformer;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ApiArticlesController
{
    public function __construct(
        protected ArticleService $articleService,
        protected CommentService $commentService,
        protected SingleArticleTransformer $singleArticleTransformer,
        protected ClimatePageService $climatePageService,
    ) {
    }

    /**
     * @param ArticlesListRequest $request
     * @return JsonResponse
     * Get list of all Articles
     */
    public function list(ArticlesListRequest $request): JsonResponse
    {
        $limit = $request->validated()['limit'] ?? config('settings.articles.listLimit');

        $data = cached(
            fn() => $this->articleService->getListPaginate($limit),
            cacheKey('articles:list', ['limit' => $limit]),
            $this->articleService->getCacheTags(),
            3600
        );

        return ApiListResponse::create($data)
            ->setTransformer(ListTransformer::class)
            ->toJsonResponse();
    }

    /**
     * @param ArticleCategory $category
     * @return JsonResponse
     * Get Articles list by Category
     */
    public function categoryList(ArticleCategory $category): JsonResponse
    {
        $perPage = (int) config('settings.articles.listLimit');

        $data = $this->articleService->getCategoryListPaginate($category, $perPage);

        return ApiListResponse::create($data)
            ->setTransformer(ListTransformer::class)
            ->setData(['category' => (new CategoryTransformer)->transform($category)])
            ->toJsonResponse();
    }

    /**
     * @param Article $article
     * @return JsonResponse
     * Get single Article
     */
    public function singleArticle(Article $article): JsonResponse
    {
        $article->loadCount('publishedComments');

        return response()->json(
            $this->singleArticleTransformer->transform($article),
        );
    }

    public function climatePageArticleList(Request $request): JsonResponse
    {
        $limit = (int)$request->input('limit', config('settings.articles.climatePageListLimit'));

        $data = cached(
            fn() => $this->climatePageService->getClimatePageArticleList($limit),
            cacheKey('articles:climate-page:list', ['limit' => $limit]),
            $this->climatePageService->getCacheTags(),
            3600
        );

        return ApiListResponse::create($data)
            ->toJsonResponse();
    }

    public function listComments(Article $article, ListComments $request): JsonResponse
    {
        $limit           = (int)$request->input('limit', config('settings.articles.commentsListLimit'));
        $commentParentId = $request->input('parent_id');

        $cacheTags = $this->commentService->getModelCacheTags(Comment::MODEL_TYPE_ARTICLE);

        $data = cached(
            fn() => $this->commentService->getListPaginate(Comment::MODEL_TYPE_ARTICLE, $article->id, $commentParentId, $limit),
            cacheKey(sprintf('article:comments:list:%d', $article->id), $request->validated()),
            $cacheTags,
            3600
        );

        return ApiListResponse::create($data)
            ->setTransformer(CommentListTransformer::class)
            ->toJsonResponse();
    }

    public function addComment(Article $article, StoreComment $request): JsonResponse
    {
        if (false === $article->is_comments_allowed) {
            abort(400, 'Add comment to article is not allowed.');
        }

        $this->commentService->store(Comment::MODEL_TYPE_ARTICLE, $article->id, $request->getSanitized());

        return response()->json([
            'result' => true,
        ]);
    }
}
