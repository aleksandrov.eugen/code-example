<?php

namespace App\Services;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Repositories\ArticleRepository;
use App\Services\Search\Index\IndexInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

final class ArticleService
{
    public function __construct(
        private ArticleRepository        $articleRepository,
        private GoogleTranslationService $googleTranslationService,
        private ModelSearchIndexer       $modelSearchIndexer,
    ) {
    }

    public function getCacheTags(): array
    {
        return ['article'];
    }

    public function getLabels(): Collection
    {
        return collect(Article::getLabels())
            ->map(function ($value, $key) {
                return [
                    'name' => $value,
                    'code' => $key,
                ];
            })->values();
    }

    public function getTranslations(Article $article): Collection
    {
        return $this->articleRepository->getTranslations($article);
    }

    public function getListPaginate(int $limit): LengthAwarePaginator
    {
        return $this->articleRepository->getListPaginate($limit);
    }

    public function fillTitleTranslation(Article $article): void
    {
        // Translate Title using Google and fill in title_en
        $titleEn = $this->googleTranslationService->makeTranslation($article->title, 'en');

        if (!empty($titleEn)) {
            $article->title_en = $titleEn;
        }
    }

    public function reindexLinkMasterIndex(Article $article): void
    {
        $this->modelSearchIndexer->indexItem(
            IndexInterface::INDEX_LINK_MASTER,
            $article
        );
    }

    public function getCategoryListPaginate(ArticleCategory $category, ?int $perPage = 20): LengthAwarePaginator
    {
        return $this->articleRepository->getCategoryListPaginate($category, $perPage);
    }
}
