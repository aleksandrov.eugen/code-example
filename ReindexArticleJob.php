<?php

namespace App\Jobs;

use App\Models\Article;
use App\Services\ArticleService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

/**
 * Translate Title using Google and fill in title_en.
 */
class ReindexArticleJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(protected int $articleId)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function handle(): void
    {
        /** @var Article $article */
        $article = Article::findOrFail($this->articleId);

        /** @var ArticleService $articleService */
        $articleService = app()->make(ArticleService::class);

        $articleService->reindexLinkMasterIndex($article);
    }
}
